import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {
  lightTheme: boolean = true;
  showFiller = false;

  constructor() { }

  ngOnInit() {
  }

  changeTheme(lightColor: boolean) {
    if (lightColor)
      document.documentElement.setAttribute('theme', 'light');
    else
      document.documentElement.setAttribute('theme', 'dark');
  }

}
